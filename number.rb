# encoding: utf-8
def phone_to_number(phone)
    arr = [
        ['','','',''],
        ['A','B','C',''],
        ['D','E','F',''],
        ['G','H','I',''],
        ['J','K','L',''],
        ['M','N','O',''],
        ['P','Q','R','S'],
        ['T','U','V',''],
        ['W','X','Y','Z'],
        ['','','','']
    ]

    number_split = phone.split('')
    result = '555'

    10.times do |i|
        4.times do |j|
            if number_split[i] == arr [i][j]
                ii = i + 1
                result<<ii.to_s
            end
        end
    end
    return result
end

puts phone_to_number('555MATRESS')